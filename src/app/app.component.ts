import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import { Component,OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import{map,startWith} from 'rxjs/operators'
import {MatSnackBar} from '@angular/material/snack-bar'
import{MatDialog} from '@angular/material/dialog'
import { DialogExampleComponent } from './dialog-example/dialog-example.component';
import { MatSort } from '@angular/material/sort';

import{ MatTableDataSource} from '@angular/material/table'
import{MatPaginator} from '@angular/material/paginator'



export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];

/**
 * @title Basic use of `<table mat-table>`
 */

export class TableBasicExample {
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource =  new MatTableDataSource(ELEMENT_DATA);
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  implements OnInit{
  title = 'Material-demo';
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol'];
  dataSource = ELEMENT_DATA;
constructor(private snackBar: MatSnackBar,public dialog: MatDialog){}
  logChange(index:any){
    console.log(index)
  }
  notifications = 2;
  showSpinner = false;
  loadData(){
    this.showSpinner = true;
    setTimeout(()=>{
      this.showSpinner = false
    },5000)
  }

  ngOnInit(): void {

    this.filteredOptions=this.myControl.valueChanges.pipe(
      startWith(''),
      map(value=> this._filter(value))
    )
    this.dataSource.sort=this.sort
    this.dataSource.paginator= this.paginator
  }
  private _filter(value:string):string[]{
    const filterValue = value.toLowerCase();
    return this.options.filter(option=>
      option.toLowerCase().includes(filterValue))
    }
  opened= false;

  log(state:string){
    console.log(state)
  }

  myControl=new FormControl();
  filteredOptions!:Observable<string[]>

  options: string[]=['Angular','React','vue']
  objectOptions=[
    {name:'angular'},
    {name:'Angular Material'},
    {name:'React'},
    {name:'Vue'}
  ]
  
  openDialogbox(){
    let dialogref= this.dialog.open(DialogExampleComponent,{data:{name:'Vishwas'}})
    dialogref.afterClosed().subscribe(result=>{
      console.log(`Dialog result ${result}`)
    })
  }
  openDialog(){
this.dialog.open(DialogExampleComponent)
  }
  openSnackBar(message:string,action:string){
    let snackBarRef =this.snackBar.open(message,action,{duration:2000})
    snackBarRef.afterDismissed().subscribe(()=>{
      console.log('The snackbar was dismissed')
    })

}
applyFilter(filterValue:string){
  this.dataSource.filter= filterValue.trim().toLowerCase()
}

@ViewChild(MatSort)sort:MatSort
@ViewChild(MatPaginator) paginator: MatPaginator


for(let i=0;i<1000;i++){
  this.numbers.push(i)
}
}





