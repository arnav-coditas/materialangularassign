import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import { MaterialModule } from './material/material.module';
import { FormsModule} from '@angular/forms'
import { ReactiveFormsModule} from '@angular/forms'
import { DialogExampleComponent } from './dialog-example/dialog-example.component';
import {ScrollingModule} from '@angular/cdk/scrolling'

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
  MaterialModule,
  FormsModule,
  ReactiveFormsModule,
  ScrollingModule

   
  
  ],
entryComponents:[DialogExampleComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
